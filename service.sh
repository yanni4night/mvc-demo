
if [ "$1" = 'start' ];then
    forever -c supervisor start bin/www
elif [ "$1" = 'stop' ];then
    forever -c supervisor stop bin/www
fi