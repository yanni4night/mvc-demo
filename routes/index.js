var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.redirect('/index.html');
});

router.get(/\.html$/, function(req, res) {
    res.render(req.path.slice(1, -5), {
        title: 'Demo for MVC frameworks'
    });
});

module.exports = router;