/**
 * Copyright (C) 2014 yanni4night.com
 * index.js
 *
 * changelog
 * 2014-12-04[19:33:10]:revised
 *
 * @author yanni4night@gmail.com
 * @version 0.1.0
 * @since 0.1.0
 */
window.App = Ember.Application.create();

App.ApplicationController = Ember.Controller.extend({
    title: 'you are running at ember.js',
    gruntBadge: 'https://cdn.gruntjs.com/builtwith.png',
    valid: true,
    count: 50,
    checked: false,
    countChanged: function() {
        this.toggleProperty('checked', this.get('count') > 100);
    }.observes('count'),
    actions: {
        toggle: function(argument) {
            this.toggleProperty('valid');
        }
    }
});